﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.*;
	import flash.text.TextField;
	import fl.transitions.easing.*;
	import fl.transitions.*;
	import Game;
	import HatPress;
	
	
	
	//class implements simple drag and drop
	//for demonstration purposes it also communicates with the TextPanel singleton
	
	public class Draggable extends MovieClip{
		
		public var scoreamount:int;
		var HUDvalue: int;
		var tempx:Number;
		var tempy:Number;
		var moveOn:Boolean = false;
		var txt:TextField = new TextField;
		var tooltip:Tooltip = new Tooltip();
		var objectTag:String;
		/*var conarray:Array = new Array [1,2,3,4,5,6,7,8,9,10,11,12];
		var compval:Array = new Array [1,2,3];*/
		var compval:int;
		var conarray:int;
		var yellowpurple:int;
		var blueflare:int;
		var bowler:int;
		var tophat:int;
		var yellowflare:int
		public var score = new Score
		
		//txtOutput.mouseEnabled=false;

		public function Draggable() {
			// constructor code
			//trace(Game.getpress());
			objectTag = "taggy";
			buttonMode = true;
			this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
			this.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
			
			
			do
			{	
				tempx = Math.random()*600;
				tempy = Math.random()*450;
				if(tempy>185&& tempx>300){moveOn=true;}
				
			}
			while(!moveOn)
			
			this.x=tempx;
			this.y=tempy;
			
			this.addEventListener(MouseEvent.MOUSE_OVER,overMouse);
			this.addEventListener("mouseOut", mouseRollOut);
		}

		public function overMouse(e:MouseEvent):void {
			addChild(tooltip);
			tooltip.txt.text = objectTag;
			var myTween:Tween = new Tween(tooltip, "alpha", Regular.easeIn, 0, 1, 0.5, true);
			
		}
		function mouseRollOut(e:MouseEvent):void {
			removeChild(tooltip);
		}
		
		public function getname():String
		{
			return "Draggable Item";
		}
		
		public function mouseDownListener(e:Event)
		{
			this.startDrag();
			//removeChild(tooltip);
		}
		
		public function mouseUpListener(e:Event)
		{
		this.stopDrag();
		//addChild(tooltip);
		if (e.target.hitTestObject(Game.getpress())) 
	
		{
			Game.getpress().addmaterial(e.target);
			//Game.getpress().addmaterial2(e.target);
			//Game.getpress().addmaterial3(e.target);
		}
		
		
		
		}
		
		
		
	}
	
}
