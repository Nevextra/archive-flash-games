﻿package  {
	import flash.display.MovieClip;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.text.TextField;
	import fl.transitions.easing.*;
	import fl.transitions.*;

	public class Game extends MovieClip{
		public var scoreamount:int;
		public static var teststring:String;
		public var menu:MovieClip;
		public var but:SButton;
		public var but2:IButton;
		public var item:Draggable;
		var purplePlait = new PurplePlait;
		public var txt:TextField = new TextField ;
		public var txt2:TextField = new TextField;
		var greenPlait = new GreenPlait;
		var blackThread = new BlackThread;
		var blueFlat = new BlueFlat;
		var blueScrunchie = new BlueScrunchie;
		var blueThread = new BlueThread;
		var pThread = new PThread;
		var redScrunchie = new RedScrunchie;
		var rgThread = new RgThread;
		var strawBundle = new StrawBundle;
		var yellowFlat = new YellowFlat;
		var hud = new HUD;
		var fBackground = new FBackground;
		var fback = new FBack
		var sSplitter = new StrawSplitter
		var hStretcher = new Hatstretcher
		var animation = new Animation
		public static var hatPress = new HatPress
		var tooltip:Tooltip = new Tooltip();
		var back = new BackButton
		var redScrunch2 = new redSchunchie2
		var yellowScrunch = new yellowScrunchie
		var feather = new YellowGFeather
		var blackf = new BlackFlat
		var redbread = new RedBreadPlait
		var instructions = new Instructions
		public static var pressAni = new HatPressSWF
		public var score = new Score
		


		public function Game() 
		{
			
			ShowMenu();
			
		}
		
		
		
		public static function getpress():MovieClip
		{
			return pressAni;
		}
		public function ShowMenu()
		
		{
			menu = new Menu();
			addChild(menu);
			
			but = new SButton();
			addChild(but);
			but.x=100;
			but.y=500;
			
			but2 = new IButton();
			addChild(but2);
			but2.x=450;
			but2.y=500;
			
			but.addEventListener(MouseEvent.MOUSE_DOWN,menumousedownListener);
			but2.addEventListener(MouseEvent.MOUSE_DOWN,menu2mousedownListener);
			but.buttonMode = true
			but2.buttonMode = true
			
			
		}
		public function menumousedownListener(e:Event)
		{
			HideMenu()
			ShowGame()
		}
		
		
		public function menu2mousedownListener(e:Event)
		{
			HideMenu()
			ShowInstructions()
			
		}
		
		public function HideMenu()
		{
			removeChild(menu);
			removeChild(but);
			removeChild(but2);
			
			//but.removeEventListener(MouseEvent.MOUSE_DOWN,instructionsMenuListener);
		}
		
		public function ShowInstructions()
		{
			/*addChild(txt)
			txt.text=("Drag the required materials into the hat press in order to make a hat, if you get it wrong you will make a rubbish hat!")
			txt.autoSize = "left";
			txt.Font:Cambria;
			addChild (animation);
			animation.width = 600
			animation.height= 450
			animation.x = 200
			animation.y = 150*/
			addChild(instructions)
			instructions.width = 799
			instructions.height = 599
			
			
			addChild (back);
			back.addEventListener(MouseEvent.MOUSE_DOWN,backmousedownListener);
			back.buttonMode = true
		}
		
		public function backmousedownListener(E:Event)
		{
			removeChild(instructions);
			ShowMenu();
			
			
		}
		
		public function ShowGame()
		{
			//addChild(fBackground);
			addChild(fback)
			addChild(hud);
			HatPressSWF();
			Points();
			fback.y = 100
			addChild(hStretcher);
			hStretcher.x = 623
			hStretcher.y = 500
			addChild(sSplitter);
			sSplitter.x = 690
			sSplitter.y = 450
			addChild(txt);
			addChild(purplePlait);
			addChild(greenPlait);
			addChild(blackThread);
			addChild(blueFlat);
			addChild(blueScrunchie);
			addChild(blueThread);
			addChild(redScrunchie);
			addChild(strawBundle);
			addChild(yellowFlat);
			addChild (txt2)
			addChild(redScrunch2);
			addChild(yellowScrunch);
			addChild(feather);
			addChild(redbread);
			addChild(blackf);
			txt2.x = 900
			txt2.y = 400
			txt2.text=("override meh")
			sSplitter.addEventListener("mouseOver", mouseRollOver);
			sSplitter.addEventListener("mouseOut", mouseRollOut);
			sSplitter.addEventListener("mouseMove", mouseMove1);
			trace('Game')
			
			
			
		}
		
		public function Points()
		{
			addChild(score);
			score.x=549
			score.y=542
		}
		
		
		public function HatPressSWF()
		{
			addChild(pressAni)
			pressAni.stop();
			pressAni.x=-30
			pressAni.y=315
		}
		
		function mouseRollOver(e:MouseEvent):void {
			addChild(tooltip);
			tooltip.txt.text = "Straw Splitter";
			var myTween:Tween = new Tween(tooltip, "alpha", Regular.easeIn, 0, 1, 0.5, true);
	
		}
		
		function mouseRollOut(e:MouseEvent):void {
			removeChild(tooltip);
		}
		
		function mouseMove1(e:MouseEvent):void 
		{
			tooltip.x = stage.mouseX;
			tooltip.y = stage.mouseY - sSplitter.height;
		}
			
		
	
	}
	
}
