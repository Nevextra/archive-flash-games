﻿package  {
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	public class StatusBar extends MovieClip {

		private var myText:TextField;
		
		public function StatusBar() {
			// constructor code
			myText = new TextField();
			this.addChild(myText);
		}
		
		public static function Instance():StatusBar
		{
			if (null==instance)
			{
				instance=new StatusBar();
				
			}
			return instance;
		}
		
		
		public function SetText(txt:String)
		{
		myText.text = txt;
		}
		
		private static var instance:StatusBar = null;

	}
	
}
