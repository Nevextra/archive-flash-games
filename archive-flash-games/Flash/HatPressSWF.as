﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import fl.motion.easing.Back;
	import fl.transitions.*;
	
	
	public class HatPressSWF extends MovieClip
	{
		public var material1:MovieClip;
		public var material2:MovieClip;
		public var material3:MovieClip;
		public var matcount:int;
		public var scoreValue:int;
		public var score = Score
		
		
		
		public function HatPressSWF() {
			// constructor code
			matcount=0;
			this.addEventListener(MouseEvent.MOUSE_DOWN, downListener);
			//this.addEventListener(Event.ENTER_FRAME,matvals);
			

		}
		
		public function addmaterial(_mat:MovieClip)
			{
				if(matcount==0)
				{
					material1 = _mat;
					matcount++;
					this.parent.removeChild(material1);
				}
				else if(matcount==1)
				{
					if(_mat==material1)
						{
							matcount=1
						}
					else
						{
							material2 = _mat;
							matcount++;
							this.parent.removeChild(material2);
						}
				}
				else if(matcount==2)
				{
					
					if(_mat==material1&&_mat==material2)
						{
							matcount=2
						}
					else
						{
							material3 = _mat;
							matcount++;
							this.parent.removeChild(material3);
						}
				}
				
			
				if(matcount==3)
				{
					trace("there are three materials");
					trace(material1);
					trace(material2);
					trace(material3);
				
					var sum = material1.compval+material2.compval+material3.compval;
					trace(sum)
					
					trace("Starting order");
					trace(material1.compval);
					trace(material2.compval);
					trace(material3.compval);
				
				}
			
			}
			
		//public function matvals (e:Event)
//			{
//				
//				
//				
//			}
		
		public function downListener(e:MouseEvent)
			{
				if(matcount ==3)
				{
					
					
					var sortArray:Array = [material1.compval,material2.compval,material3.compval];
					var sorted:Boolean = false;
					do
					{
						sorted  = true;
						for(var i :int = 0; i<= 2;i++)
						{
							if(sortArray[i] > sortArray[i+1])
							{
								var pos1:int = sortArray[i];
								var pos2:int = sortArray[i+1];
								sortArray[i] = pos2;
								sortArray[i+1] = pos1;
								sorted = false;
							}
							/*else
							{
								sorted = true;
							}*/
						}
					}
					while(!sorted)
					
					trace("the order is");
					trace(sortArray[0]);
					trace(sortArray[1]);
					trace(sortArray[2]);
					createhat()
					matcount=0
					
					this.gotoAndPlay(1);
							
				}
				
			}
			
		public function calculate(scoreValue)
		{
			score.scoreBox.text+=scoreValue
		}
			
		public function createhat()
			{
				if (material1.yellowpurple==1&&material2.yellowpurple==1&&material3.yellowpurple==1)
				{
					var yp:YellowPurpleBraidHat=new YellowPurpleBraidHat
					this.addChild(yp)
					trace("success")
					
				}
				else if (material1.blueflare==1&&material2.blueflare==1&&material3.blueflare==1)
				{
					var bf:BlueflareHat= new BlueflareHat
					this.addChild(bf)
					trace ("success")
					
				}
				else if (material1.bowler==1&&material2.bowler==1&&material3.bowler==1)
				{
					var bw:BowlerHat = new BowlerHat
					this.addChild(bw)
					trace ("success")
					calculate(scoreValue=100)
				}
				else if (material1.tophat==1&&material2.tophat==1&&material3.tophat==1)
				{
					var th:TopHat= new TopHat
					this.addChild(th)
					trace ("success")
					
				}
				else if (material1.yellowflare==1&&material2.yellowflare==1&&material3.yellowflare==1)
				{
					var yf:YellowFlareHat= new YellowFlareHat
					this.addChild(yf)
					trace ("success")
					
				}
				else
				{
					var jh:JunkHat=new JunkHat
					this.addChild(jh)
					jh.height = 200
					jh.width = 200
					trace("too bad")
					
				}
				
			}
				
				/*if((material1.compval+material2.compval+material3.compval)==6)
				{
					trace("6")
					if((material1.conarray)==1&&(material2.conarray)==10&&(material3.conarray)==12)
						{
							
						}
				}*/
	
			
	}
	
}
