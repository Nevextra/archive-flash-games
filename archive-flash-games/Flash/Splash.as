﻿package  {
	
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Splash extends MovieClip {
		
		var myLoader:Loader;
		var loadingAnim:LoadingAnimation;
		var percentLoaded:TextField =new TextField();
		
		public function Splash() {
			// constructor code
			myLoader = new Loader();
			myLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			
			/*var blubox = new BluBox();
			addChild(blubox);
			blubox.x=200;
			blubox.y=200;*/
			
			var loadingScreen = new loadingscrn();
			addChild(loadingScreen);
			/*loadingScreen.x=200;
			loadingScreen.y=200;*/
			
			loadingAnim = new LoadingAnimation();
			addChild(loadingAnim);
			loadingAnim.x=200;
			loadingAnim.y=495;
			loadingAnim.scaleX=0;
			
			var format:TextFormat = new TextFormat();
            format.font = "Verdana";
            format.color = 0xFF0000;
            format.size = 20;
            format.underline = false;

            percentLoaded.defaultTextFormat = format;
			
			addChild(percentLoaded);
			percentLoaded.x=180;
			percentLoaded.y=490;
			myLoader.load(new URLRequest("Game.swf"));
		}
		
		function onProgress(evt:ProgressEvent):void {   
 
			var nPercent:Number = Math.round((evt.bytesLoaded / evt.bytesTotal) * 100);    
			loadingAnim.scaleX = nPercent / 100;
			percentLoaded.text = nPercent.toString() + "%";
			trace("load%"+nPercent.toString());
 		}
		
		function onComplete(evt:Event):void {
			myLoader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
			myLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete); 
			loadingAnim.x=1000;
			addChild(myLoader);
		}
		
	
		
		function onIOError(evt:IOErrorEvent):void {
			trace("IOError loading SWF");
			}
	}
	
}



5	 
6	
7	 
8	
11	 
12	
13	 
14	
20	 

25	 
26